﻿using System;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private Transform _head;
    [SerializeField] private Transform _tail;
    [SerializeField] private BoxCollider2D _trailCollider;
    [SerializeField] private ParticleSystem _hit;
    [SerializeField] private Transform _headEndPoint;

    private bool _isInitialized;
    private Vector2 _startingPosition;
    private Vector2 _direction;
    public event Action<Bullet, GameObject> OnDestroyed;
    
    public void Init(Vector2 direction)
    {
        _isInitialized = true;
        _direction = direction;

        _startingPosition = transform.position;
        var angle = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle - 90, Vector3.forward);
    }

    public void Destroy(GameObject gO)
    {
        OnDestroyed?.Invoke(this, gO);
        GameObject.Destroy(gameObject);
        if (gO.CompareTag("wall"))
        {
            var p = Instantiate(_hit, _headEndPoint.position, transform.rotation);
            GameManager.Instance.ShakeCamera(0.5f);
            Destroy(p.gameObject, 1f);
        }
    }
    
    private void Update()
    {
        if(!_isInitialized) { return; }
        
        MoveHead();
        SetTail();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player") || other.CompareTag("bullet") || other.CompareTag("powerup") || other.CompareTag("exit")) { return; }
        Destroy(other.gameObject);
    }

    private void MoveHead()
    {
        _head.Translate(_direction * GameManager.Instance.GameSettings.BulletSettings.MovementSpeed * Time.deltaTime, Space.World);
    }

    private void SetTail()
    {
        var xCenter = (_startingPosition.x + _head.position.x) / 2f;
        var yCenter = (_startingPosition.y + _head.position.y) / 2f;
        var tailPos = _tail.transform.position = new Vector2(xCenter, yCenter);
        _trailCollider.size = new Vector2(_trailCollider.size.x, Vector2.Distance(_head.position, _startingPosition));
    }

    [Serializable]
    public class Settings
    {
        public float MovementSpeed;
    }
}


