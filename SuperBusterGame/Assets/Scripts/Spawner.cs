﻿using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;

public class Spawner :SerializedMonoBehaviour
{
    [SerializeField]
    private Dictionary<int, Monster> _monsters;

    [SerializeField]
    private Soul _soulPrefab;
    
    public void SpawnNewMonsters(Monster monster)
    {
        int newLvl = monster.Lvl - 1;
        var toSpawn = _monsters[newLvl];

        var move = monster.GetComponent<ReflectMove>();
        Debug.Assert(move != null, "Monster doesn't have ReflectMove component");
        SpawnNew(monster.transform, toSpawn, move.Direction.Rotate(90f));
        SpawnNew(monster.transform, toSpawn, move.Direction.Rotate(-90f));
    }

    private void SpawnNew(Transform monster, Monster toSpawn, Vector2 dir)
    {
        var m1 = Instantiate(toSpawn, monster.parent) as Monster;
        m1.transform.position = monster.position.ToVector2() + dir * .4f;
        SetDirection(m1, dir);
    }

    public Soul SpawnSoul(Vector2 pos, Transform parent)
    {
        var s = Instantiate(_soulPrefab, parent) as Soul;
        s.transform.position = pos;
        return s;
    }

    private void SetDirection(Monster m, Vector2 dir)
    {
        var move = m.GetComponent<ReflectMove>();
        Debug.Assert(move != null, "Monster doesn't have ReflectMove component");
        move.SetDirection(dir);
    }
}
