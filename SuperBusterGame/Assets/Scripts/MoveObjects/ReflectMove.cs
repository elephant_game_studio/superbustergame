﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflectMove : MonoBehaviour
{
    public static List<ReflectMove> allReflects = new List<ReflectMove>();
    public static event Action<ReflectMove> OnNewReflectMove;

    private static void AddNewReflectMove(ReflectMove reflectMove)
    {
        allReflects.Add(reflectMove);
        OnNewReflectMove?.Invoke(reflectMove);
    }

    private static void RemoveReflectMove(ReflectMove reflectMove)
    {
        allReflects.Remove(reflectMove);
    }

    [SerializeField] private float _speed;
    [SerializeField] private Rigidbody2D _rb2D;
    public Vector2 Direction { get; private set; }
	// Use this for initialization
	private void Awake () {
        Direction = RandomDirection();
        AddNewReflectMove(this);
	}

    private bool _freez;
    public bool Freez
    {
        get { return _freez; }
        set
        {
            _freez = value;
            _rb2D.isKinematic = _freez;

            if (_freez) _rb2D.velocity = Vector2.zero;
        }
    }

    private Vector2 RandomDirection()
    {
        return UnityEngine.Random.insideUnitCircle.normalized; 
    }

    public void SetDirection(Vector2 direction)
    {
        Direction = direction;
    }

    // Update is called once per frame
    void Update ()
    {
        if (Freez) return;
        transform.position = transform.position + ToVector3(Direction * _speed * Time.deltaTime);
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("wall"))
        {
            Direction = Vector2.Reflect(Direction, collision.contacts[0].normal);
        }
    }

    private static Vector3 ToVector3(Vector2 vector2)
    {
        return new Vector3(vector2.x, vector2.y, 0f);
    }

    private void OnDestroy()
    {
        RemoveReflectMove(this);
    }
}
