﻿using UnityEngine;

public class MonsterAnimator : MonoBehaviour
{
    [SerializeField] private ReflectMove _move;
    [SerializeField] private Animator _animator;
    [SerializeField] private Transform _viewRoot;

    private const string toFront = "to_front";
    private const string toBack = "to_back";



    private bool _facingRight;

    private void SetFlipX(float dir)
    {
        Vector3 scale = new Vector3(dir, 1f, 1f);
        transform.localScale = scale;
    }

    private void Update()
    {
        SetFlipX((_move.Direction.y > 0f ? 1 : -1f) * Mathf.Sign(_move.Direction.x));
        _animator.ResetTrigger(toFront);
        _animator.ResetTrigger(toBack);
        _animator.SetTrigger(_move.Direction.y > 0f ? toBack : toFront);
    }
}
