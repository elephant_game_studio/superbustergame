﻿using DG.Tweening;
using DG.Tweening.Plugins.Core.PathCore;
using Sirenix.OdinInspector;
using System;
using UnityEngine;

public class Soul : MonoBehaviour
{

    [ShowInInspector]
    public void StartJoruney(Vector2 destination)
    {
        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one, .4f);
        Vector2 start = transform.position;
        Vector2 mid = start + (destination - start) / 2f;
        mid += UnityEngine.Random.insideUnitCircle;
        Path path = new Path(PathType.CatmullRom, new Vector3[] { start, mid, destination }, 10);
        transform.DOPath(path, 2f);
    }

    
    private void DestoyNow()
    {
        Destroy(gameObject);
    }

    [ShowInInspector]
    public void DestroyMe()
    {
        //DestoyNow();
        //transform.DOMoveY(transform.position.y + .7f, .8f).SetEase(Ease.OutExpo);
        transform.DOScale(0f, .8f).OnComplete(DestoyNow);
        //_sprite.DOFade(0f, .9f).OnComplete(DestoyNow);
    }

    public void StartJoruneyToDoors()
    {
        StartJoruney(GameManager.Instance.Doors.position);
    }
}
