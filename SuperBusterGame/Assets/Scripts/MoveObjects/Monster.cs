﻿using Sirenix.OdinInspector;
using System;
using DG.Tweening;
using UnityEngine;

public class Monster : MonoBehaviour
{
    [SerializeField] private int _lvl;
    [SerializeField] private GameObject _explosionEffect;
    public int Lvl => _lvl;
    public event Action<Monster> OnKill;

    private void Start()
    {
        KillManager.Instance.RegisterMe(this);
    }

    [ShowInInspector]
    private void Kill()
    {
        GameManager.Instance.ShakeCamera(0.3f);
        OnKill?.Invoke(this);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            Kill();
        }
    }

    private void CreateEffect()
    {
        if (_explosionEffect == null) { return; }

        var effect = Instantiate(_explosionEffect, transform.parent);
        effect.transform.position = transform.position;
        DOVirtual.DelayedCall(1f, () => Destroy(effect.gameObject));
    }
    
    public void DestroyMe()
    {
        CreateEffect();
        Destroy(gameObject);
    }
}
