﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TutorialBehaviour : MonoBehaviour
{
	[SerializeField] private SpriteRenderer[] _elementsToShow;
	[SerializeField] private SpriteRenderer[] _elementsToHide;

	private void Start()
	{
		GameManager.Instance.SoulsCounter.OnLevelClear += Animate;
	}

	private void OnDestroy()
	{
		GameManager.Instance.SoulsCounter.OnLevelClear += Animate;
	}

	private void Animate()
	{
		foreach (var element in _elementsToShow)
		{
			element.DOFade(0.5f, 1f);
		}

		foreach (var element in _elementsToHide)
		{
			element.DOFade(0f, 1f);
		}
	}
}
