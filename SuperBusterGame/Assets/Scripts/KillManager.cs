﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillManager : MonoSingleton<KillManager>
{
    [SerializeField] private Spawner _spawner;

    public event Action<Monster> OnNewMonsterKill;
    //public int Killed { get; private set; }

    public void RegisterMe(Monster monster)
    {
        monster.OnKill += OnMonsterKilled;
    }

    private void OnMonsterKilled(Monster monster)
    {
        OnNewMonsterKill?.Invoke(monster);
        if (monster.Lvl == 1)
        {
            SpawnSoul(monster.transform.position, monster.transform.parent);
        }

        else
        {
            _spawner.SpawnNewMonsters(monster);
        }
        
        monster.OnKill -= OnMonsterKilled;
        monster.DestroyMe();
    }

    private void SpawnSoul(Vector2 pos, Transform parent)
    {
        var s = _spawner.SpawnSoul(pos, parent);
        s.StartJoruneyToDoors();
    }
}
