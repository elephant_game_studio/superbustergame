﻿using UnityEngine;

public abstract class ShowHide : MonoBehaviour
{
    public void Show()
    {
        gameObject.SetActive(true);
    }


    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
