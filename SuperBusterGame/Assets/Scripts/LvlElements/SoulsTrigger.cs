﻿using UnityEngine;

public class SoulsTrigger : MonoBehaviour
{
    private SoulsCounter Counter => GameManager.Instance.SoulsCounter;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("soul"))
        {
            var soul = collider.gameObject.GetComponent<Soul>();
            Debug.Assert(soul != null);
            Counter.DecresSouls();
            soul.DestroyMe();
        }
    }
}
