﻿using UnityEngine;

public class Exit : MonoBehaviour
{
    [SerializeField] private ShowHide _doors;
    [SerializeField] private ShowHide _ladder;

    private void Awake()
    {
        GameManager.Instance.SoulsCounter.OnLevelClear += LevelClear;
    }

    private void OnDestroy()
    {
        if (GameManager.HasInstance)
            GameManager.Instance.SoulsCounter.OnLevelClear -= LevelClear;
    }

    private void LevelClear()
    {
        //_doors.Hide();
        _ladder.Show();
    }
}
