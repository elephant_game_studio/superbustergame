﻿using UnityEngine;
using DG.Tweening;

public class Ladder : ShowHide
{
    [SerializeField] private ParticleSystem _playerIn;

    private PlayerMovement _movement;
    private Transform PlayerTransfrom => _movement.transform;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            _movement = collision.GetComponentInParent<PlayerMovement>();
            _movement.enabled = false;
            PlayerTransfrom.DOScale(0f, .5f);
            PlayerTransfrom.DOMove(transform.position, .4f).OnComplete(() => _playerIn.gameObject.SetActive(true));
            Invoke(nameof(CallWin), .9f);
            //CallWin;
        }
    }

    private void CallWin()
    {
        PlayerTransfrom.localScale = Vector3.one;
        _movement.enabled = true;
        GameManager.Instance.WinGame();
    }
}
