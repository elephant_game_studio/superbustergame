﻿using System;
using UnityEngine;

public class ScoreViewSpawner : MonoBehaviour
{
    [SerializeField] private Score _score;
    [SerializeField] private SceneScoreView _scoreView;

    private void Awake()
    {
        _score.NewScoreOnPosition += SpawnScore;
    }

    private void SpawnScore(Vector2 pos, int value)
    {
        var scoreView = Instantiate(_scoreView);
        scoreView.transform.position = pos;
        scoreView.gameObject.SetActive(true);
        scoreView.Value = value;
        scoreView.ShowMe();
    }
}
