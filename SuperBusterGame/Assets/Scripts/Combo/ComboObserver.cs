﻿using System;
using UnityEngine;

public class ComboObserver : MonoBehaviour
{
    private PlayerShooting _shooting;

    public event Action<int> OnKillStrikeUpdate;
    private int _killStrike;
    public int KillStrike
    {
        get { return _killStrike; }
        private set
        {
            _killStrike = value;
            OnKillStrikeUpdate?.Invoke(_killStrike);
        }
    }

    private void Awake()
    {
        GameManager.Instance.OnNewLevelLoaded += NewLevelLoaded;
    }

    private void NewLevelLoaded(int obj)
    {
        KillStrike = 0;
        var shooting = GameManager.Instance.Player.GetComponent<PlayerShooting>();
        Debug.Assert(shooting != null);
        SetNewShooter(shooting);
    }

    private void SetNewShooter(PlayerShooting shooter)
    {
        if (_shooting != null)
            _shooting.OnNewBullet -= NewBullet;
        _shooting = shooter;
        _shooting.OnNewBullet += NewBullet;
    }

    private void NewBullet(Bullet bullet)
    {
        bullet.OnDestroyed += CheckDestroy;
    }

    public void ResetCombo()
    {
        KillStrike = 0;
    }

    private void CheckDestroy(Bullet b, GameObject obj)
    {
        if (obj.CompareTag("monster"))
        {
            KillStrike++;
        }
        else
        {
            ResetCombo();
        }
        b.OnDestroyed -= CheckDestroy;
    }
}


