﻿using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ComboUI : MonoBehaviour
{
    [SerializeField] private Text _strikeValue;
    [SerializeField] private ComboObserver _combo;

    private int _prevStrikeValue = 0;

    private void Start()
    {
        KillStrikeUpdate(_combo.KillStrike);
        _combo.OnKillStrikeUpdate += KillStrikeUpdate;
    }

    private void KillStrikeUpdate(int strike)
    {
        if (strike > 1)
        {
            if (_prevStrikeValue == 1) ShowStrike();
            DoPositiveUpdate(strike);
        }
        else
            HideStrike();
        _strikeValue.text = "x" + strike.ToString();
        _prevStrikeValue = strike;
    }

    private void HideStrike()
    {
        transform.DOScale(Vector3.zero, .5f);
    }

    private void ShowStrike()
    {
        transform.DOScale(Vector3.one, .5f);
    }

    private Sequence _sequence;

    [ShowInInspector]
    private void DoPositiveUpdate(int strike)
    {
        var force = Mathf.Clamp(strike, 0, 4);
        if (_sequence != null) _sequence.Kill();
        _sequence = DOTween.Sequence();
        _sequence
            .Append(_strikeValue.transform.DOPunchRotation(Vector3.forward * .7f * force, .6f))
            .Insert(0, _strikeValue.transform.DOPunchScale(Vector3.one * (.1f * force), .7f))
            ;
    }
}


