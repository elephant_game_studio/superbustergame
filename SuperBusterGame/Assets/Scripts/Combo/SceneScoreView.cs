﻿using DG.Tweening;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SceneScoreView : MonoBehaviour
{
    [SerializeField] private Text _tmp;
    [SerializeField] private CanvasGroup _canvasGroup;

    private int _value;
    public int Value
    {
        get { return _value; }
        set
        {
            _value = value;
            _tmp.text = "+" + _value;
        }
    }



    Sequence _sequence;

    [ShowInInspector]
    public void ShowMe()
    {
        ResetIfRun();
        _tmp.transform.localScale = Vector3.zero;
        _sequence = DOTween.Sequence();

        _sequence.Append(_tmp.transform.DOLocalMoveY(1f, .3f))
            .Append(_tmp.transform.DOPunchScale(Vector3.one * .01f, .4f))
            .Append(_canvasGroup.DOFade(0f, .3f))
            .Insert(0, _tmp.transform.DOScale(0.04f, .3f))
            .OnComplete(() => Destroy(gameObject));
        ;
    }

    private void ResetIfRun()
    {
        if (_sequence != null)
        {
            _sequence.Kill();
            _canvasGroup.DOFade(1f, 0f);
            _tmp.transform.localPosition = Vector3.zero;
        }
    }
}