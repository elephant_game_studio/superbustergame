﻿using Sirenix.OdinInspector;
using System;
using UnityEngine;

public class Score : MonoBehaviour
{
    [SerializeField] private ComboObserver _comboObserver;
    [SerializeField] private int _valuePerLevel;
    [SerializeField] private int _maxMonsterLevel;

    public event Action<int> ScoreUpdate;
    public event Action<Vector2, int> NewScoreOnPosition;

    private int _scoreValue;
    [ShowInInspector]
    public int ScoreValue
    {
        get { return _scoreValue; }
        private set
        {
            _scoreValue = value;
            GameManager.Instance.Stats.LevelScore = value;
            ScoreUpdate?.Invoke(value);
        }
    }

    private void Start()
    {
        KillManager.Instance.OnNewMonsterKill += NewKill;
        GameManager.Instance.OnNewLevelLoaded += NewLevelLoaded;
        gameObject.SetActive(false);
    }

    private void NewLevelLoaded(int levelLoaded)
    {
        ScoreValue = 0;
        if (!gameObject.activeInHierarchy) gameObject.SetActive(true);
    }

    private int MonsterLevelToScore(int lvl)
    {
        return (int)Mathf.Pow(2, _maxMonsterLevel - lvl) * _valuePerLevel;
    }

    private void NewKill(Monster monster)
    {
        int score = MonsterLevelToScore(monster.Lvl) * Mathf.Clamp(_comboObserver.KillStrike, 1, int.MaxValue);
        NewScoreOnPosition?.Invoke(monster.transform.position, score);
        ScoreValue += score;
    }
}


