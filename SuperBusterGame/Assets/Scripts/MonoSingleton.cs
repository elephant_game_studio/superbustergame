﻿using Sirenix.OdinInspector;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;


public abstract class MonoSingleton<T> : SerializedMonoBehaviour where T : MonoSingleton<T>
{
    private static T _instance;

    /// <summary>
    /// Get (or create) the instance of this Singleton
    /// </summary>
    public static T Instance
    {
        [DebuggerStepThrough]
        get
        {
            // Instance required for the first time, we look for it
            if (!HasInstance)
            {
                //print("{0} :: {1} :: {2} :: {3}".Fmt(HasInstance ? _instance.transform.GetPath() : " -- ", _instance, Application.isPlaying, IsQuiting));
                _instance = GameObject.FindObjectOfType<T>();
                if (_instance == null)
                {
                    var go = new GameObject("_" + typeof(T).Name);
                    _instance = go.AddComponent<T>(); // _instance set by Awake() constructor
                    //print("Created new instance of {0}! {1} :: {2}".Fmt(typeof(T), _instance.transform.GetPath(), _instance.GetInstanceID()));
                }
                else
                {
                    print("Found instance of");
                }
            }
            return _instance;
        }
    }

    public static bool HasInstance
    {
        get { return !(_instance == null); }
    }

    // If no other monobehaviour request the instance in an awake function
    // executing before this one, no need to search the object.
    public void Awake()
    {
        if (_instance != null && _instance != (T)this)
        {
           DestroyImmediate(gameObject);
            return;
        }

        _instance = (T)this;

    }
}