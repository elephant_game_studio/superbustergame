﻿using System;
using System.Linq;
using UnityEngine;

public class SoulsCounter : MonoBehaviour
{
    public int SoulsMax { get; private set; }
    private int _soulsLeft;
    public int SoulsLeft
    {
        get { return _soulsLeft; }
        private set
        {
            _soulsLeft = value;
            OnSoulsUpdate?.Invoke(); }
    }

    public event Action OnSoulsUpdate;
    public event Action OnLevelClear;

    public void RefreshSoulsCounter()
    {
        var monsters = FindObjectsOfType<Monster>();
        SoulsMax = (int)monsters.Select(x => Mathf.Pow(2, x.Lvl - 1)).Sum();
        SoulsLeft = SoulsMax;
    }

    public void DecresSouls()
    {
        SoulsLeft--;
        Debug.Assert(SoulsLeft >= 0);
        if (_soulsLeft == 0)
        {
            OnLevelClear?.Invoke();
        }
    }
}
