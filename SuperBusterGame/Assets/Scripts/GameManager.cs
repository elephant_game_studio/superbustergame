﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class GameManager : MonoSingleton<GameManager>
{
    [SerializeField] private SoulsCounter _soulsCounter;
    [SerializeField] private GameSettingsSO _gameSettings;
    [SerializeField] private Player _player;
    [SerializeField] private Camera _mainCamera;
     
    public GameState GameState { get; private set; }
    public SoulsCounter SoulsCounter => _soulsCounter;
    public GameSettingsSO GameSettings => _gameSettings;


    public int Lifes
    {
        get { return Stats.Lifes; }
        private set
        {
            Stats.Lifes = value;
            OnLifesUpdate?.Invoke(Stats.Lifes);
        }
    }

    public event Action<int> OnLifesUpdate;

    public GameStats Stats => _gameStats ?? (_gameStats = new GameStats());

    public Player Player
    {
        get
        {
            return _player;
        }
    }

    private Transform _doors;
    public Transform Doors
    {
        get
        {
            if (_doors == null)
            {
                _doors = FindObjectOfType<Exit>().transform;
            }
            return _doors;
        }
        private set { _doors = value; }
    }

    public event Action<int> OnNewLevelLoaded;

    private GameStats _gameStats;
    private int _previousLevelId;
    private int _currentLevelId;

    public void StartGame()
    {
        Time.timeScale = 0f;
        _currentLevelId = 0;
        _previousLevelId = 0;
        GameState = GameState.Starting;
        Stats.GameResult = GameResult.NotDone;
        Stats.TotalScore = 0;
        Stats.TotalTime = 0f;
        UIManager.Instance.MainMenuUI.Hide();
        UIManager.Instance.CountDownUi.SetActive(false);
        StartCoroutine(LoadLevel(_gameSettings.General.Levels[_currentLevelId]));
    }

    public void WinGame()
    {
        Stats.TotalTime += Stats.LevelTime;
        Stats.TotalScore += Stats.LevelScore;
        Time.timeScale = 0f;
        Player.gameObject.SetActive(false);
        
        GameState = GameState.Results;
        _previousLevelId = _currentLevelId;
        _currentLevelId++;

        if (_currentLevelId >= GameSettings.General.Levels.Length)
        {
            Stats.GameResult = GameResult.Won;
        }
        
        UIManager.Instance.ResultScreenUi.ShowResult();
    }

    public bool PlayerHit()
    {
        Lifes--;
        if (Lifes <= 0)
        {
            _previousLevelId = _currentLevelId;
            Stats.TotalTime += Stats.LevelTime;
            Stats.TotalScore += Stats.LevelScore;
            Player.gameObject.SetActive(false);
            Time.timeScale = 0f;
            Stats.GameResult = GameResult.Failed;
            GameState = GameState.Results;
            Lifes = 0;
            UIManager.Instance.ResultScreenUi.ShowResult();
            return false;
        }
        return true;
    }

    public void Continue()
    {
        SceneManager.UnloadSceneAsync(_gameSettings.General.Levels[_previousLevelId]);
        
        if (Stats.GameResult == GameResult.Failed || Stats.GameResult == GameResult.Won)
        {
            Lifes = _gameSettings.General.InitialLifes;
            UIManager.Instance.MainMenuUI.Show();
            UIManager.Instance.ResultScreenUi.gameObject.SetActive(false);
            return;
        }
        
        UIManager.Instance.ResultScreenUi.Hide();
        StartCoroutine(LoadLevel(_gameSettings.General.Levels[_currentLevelId]));
        Debug.Log("game over win!");
    }

    public void ShakeCamera(float strength)
    {
        _mainCamera.transform.DOKill();
        _mainCamera.transform.localRotation = Quaternion.Euler(Vector3.zero);
        _mainCamera.transform.DOShakeRotation(0.3f, new Vector3(0, 0, strength), 30, 90, true);
    }
    
    private void Start()
    {
        GameState = GameState.MainMenu;
        Lifes = _gameSettings.General.InitialLifes;
        Stats.GameResult = GameResult.NotDone;
        Player.gameObject.SetActive(false);
    }
    
    private IEnumerator LoadLevel(string levelName)
    {
        Player.gameObject.SetActive(true);
        Player.transform.position = Vector3.zero;
        
        Doors = null;
        Stats.LevelTime = 0f;
        Stats.LevelScore = 0;
        SceneManager.LoadScene(levelName, LoadSceneMode.Additive);
        yield return null;
        yield return null;
        UIManager.Instance.CountDownUi.SetActive(false);
        UIManager.Instance.CountDownUi.SetActive(true);
        SoulsCounter.RefreshSoulsCounter();
        
        OnNewLevelLoaded?.Invoke(_currentLevelId);
        
        yield return new WaitForSecondsRealtime(3f);
        Time.timeScale = 1f;
        GameState = GameState.Playing;
    }

    [Serializable]
    public class GameStats
    {
        public int Lifes;
        public int LevelScore;
        public int TotalScore;
        public float LevelTime;
        public float TotalTime;
        public GameResult GameResult;
    }
}

public enum GameState
{
    MainMenu,
    Starting,
    Playing,
    Results
}

public enum GameResult
{
    NotDone,
    Won,
    Failed
}
