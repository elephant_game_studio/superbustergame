﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettingsSO", menuName = "Game/Settings")]
public class GameSettingsSO : ScriptableObject
{
    [SerializeField] private GeneralSettings _generalSettings;
    [SerializeField] private Bullet.Settings _bulletSettings;

    public GeneralSettings General => _generalSettings;
    public Bullet.Settings BulletSettings => _bulletSettings;

    [Serializable]
    public class GeneralSettings
    {
        public int InitialLifes;
        public string[] Levels;
    }
}
