﻿using UnityEngine;

public class DestroyWall : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("bullet"))
        {
            DestroyMe();
        }
    }

    public void DestroyMe()
    {
        Destroy(gameObject);
    }
}
