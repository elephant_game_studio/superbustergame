﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SoulsUI : MonoBehaviour
{
    [SerializeField] private Text _soulsLeft;
    [SerializeField] private Text _soulsMax;

    private void Start()
    {
        SoulsUpdate();
        GameManager.Instance.SoulsCounter.OnSoulsUpdate += SoulsUpdate;
    }

    private void OnDestroy()
    {
        if (GameManager.HasInstance)
            GameManager.Instance.SoulsCounter.OnSoulsUpdate -= SoulsUpdate;
    }

    private void SoulsUpdate()
    {
        _soulsLeft.text = GameManager.Instance.SoulsCounter.SoulsLeft.ToString();
        _soulsMax.text = GameManager.Instance.SoulsCounter.SoulsMax.ToString();
    }
}
