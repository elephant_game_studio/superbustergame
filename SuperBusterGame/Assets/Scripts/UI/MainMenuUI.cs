﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviour
{
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private Button _startButton;
    
    private bool _isClickable = true;

    private void OnEnable()
    {
        _startButton.Select();
    }

    public void Show()
    {
        _isClickable = true;
        _startButton.Select();
        _canvasGroup.alpha = 1;
        _canvasGroup.blocksRaycasts = true;
    }
    
    public void Hide()
    {
        _isClickable = false;
        _canvasGroup.blocksRaycasts = false;
        _canvasGroup.DOFade(0f, 0.5f);
    }
    
    
    //called from ui button
    public void StartGame()
    {
        if(!_isClickable) { return; }
        _isClickable = false;
        GameManager.Instance.StartGame();
    }
}
