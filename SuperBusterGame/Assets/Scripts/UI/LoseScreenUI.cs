﻿using DG.Tweening;
using UnityEngine;

public class LoseScreenUI : MonoBehaviour
{
    [SerializeField] private CanvasGroup _canvasGroup;
    
    private void Start()
    {
        Debug.Log("start");
    }

    public void Show()
    {
        Debug.Log("show");
        gameObject.SetActive(true);
        Debug.Log("post active");
        _canvasGroup.alpha = 0f;
        _canvasGroup.DOFade(1f, 0.5f);
        _canvasGroup.DOFade(0f, 0.5f).SetDelay(1f);
    }
}
