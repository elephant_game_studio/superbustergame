﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour
{
    [SerializeField] private Text _text;
    [SerializeField] private Score _score;

    private void Start()
    {
        SetScoreValue(_score.ScoreValue);
        _score.ScoreUpdate += SetScoreValue;
    }

    private void SetScoreValue(int score)
    {
        _text.text = score.ToString();
    }
}


