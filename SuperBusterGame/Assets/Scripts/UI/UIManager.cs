﻿using UnityEngine;

public class UIManager : MonoSingleton<UIManager>
{
    [SerializeField] private MainMenuUI _mainMenuUi;
    [SerializeField] private ResultScreenUI _resultScreenUi;
    [SerializeField] private LoseScreenUI _loseScreenUi;
    [SerializeField] private GameObject _countDownUi;

    public MainMenuUI MainMenuUI => _mainMenuUi;
    public ResultScreenUI ResultScreenUi => _resultScreenUi;
    public LoseScreenUI LoseScreenUi => _loseScreenUi;
    public GameObject CountDownUi => _countDownUi;
}
