﻿using DG.Tweening;
using UnityEngine;

public class SoulsFiller : MonoBehaviour
{
    [SerializeField] private Transform _filler;

    private void Start()
    {
        GameManager.Instance.SoulsCounter.OnSoulsUpdate += SoulsUpdate;
        _filler.localScale = Vector3.zero;
    }

    private void SoulsUpdate()
    {
        float step = 1f / GameManager.Instance.SoulsCounter.SoulsMax;
        _filler.DOScale(step * (GameManager.Instance.SoulsCounter.SoulsMax - GameManager.Instance.SoulsCounter.SoulsLeft), .8f).SetEase(Ease.OutBack);
    }

    private void OnDestroy()
    {
        if (GameManager.HasInstance)
            GameManager.Instance.SoulsCounter.OnSoulsUpdate -= SoulsUpdate;
    }
}
