﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ResultScreenUI : MonoBehaviour
{
    [SerializeField] private Text _lifesText;
    [SerializeField] private Text _levelTimeText;
    [SerializeField] private Text _generalTimeText;
    [SerializeField] private Text _levelScoreText;
    [SerializeField] private Text _generalScoreText;
    [SerializeField] private CanvasGroup[] _statsHolders;
    [SerializeField] private Button _restartButton;
    [SerializeField] private Button _continueButton;
    [SerializeField] private Button _endButton;
    [SerializeField] private GameObject _winTitle;
    [SerializeField] private GameObject _loseTitle;
    [SerializeField] private Color _winColor;
    [SerializeField] private Color _failColor;
    
    public void ShowResult()
    {
        gameObject.SetActive(true);

        SetStats();
        SetAnimation();
        SetButtons();
        SetTitle();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
    
    private void SetStats()
    {
        var stats = GameManager.Instance.Stats;
        _lifesText.text = stats.Lifes.ToString();
        _levelScoreText.text = stats.LevelScore.ToString();
        _generalScoreText.text = stats.TotalScore.ToString();

        _generalScoreText.color = stats.GameResult == GameResult.Failed ? _failColor : _winColor;
            
        var levelTime = TimeSpan.FromSeconds(GameManager.Instance.Stats.LevelTime);
        _levelTimeText.text = string.Format("{1:D2}:{2:D2}", 
            levelTime.Hours, 
            levelTime.Minutes, 
            levelTime.Seconds, 
            levelTime.Milliseconds);
        
        var generalTime = TimeSpan.FromSeconds(GameManager.Instance.Stats.TotalTime);
        _generalTimeText.text = string.Format("{1:D2}:{2:D2}", 
            generalTime.Hours, 
            generalTime.Minutes, 
            generalTime.Seconds, 
            generalTime.Milliseconds);
    }

    private void SetAnimation()
    {
        _continueButton.transform.localScale = Vector3.zero;
        _restartButton.transform.localScale = Vector3.zero;
        _endButton.transform.localScale = Vector3.zero;
        
        var delay = 0.5f;
        foreach (var element in _statsHolders)
        {
            element.alpha = 0f;
            element.transform.localScale = Vector3.one * 3;
            element.transform.DOScale(1f, 1f).SetEase(Ease.OutBack).SetDelay(delay);
            element.DOFade(1f, 0.5f).SetDelay(delay);

            delay += 0.3f;
        }

        _continueButton.transform.DOScale(1f, 0.5f).SetDelay(delay).SetEase(Ease.OutBack);
        _restartButton.transform.DOScale(1f, 0.5f).SetDelay(delay).SetEase(Ease.OutBack);
        _endButton.transform.DOScale(1f, 0.5f).SetDelay(delay).SetEase(Ease.OutBack);
        DOVirtual.DelayedCall(delay, () =>
        {
            var result = GameManager.Instance.Stats.GameResult;
            if(result == GameResult.NotDone) {_continueButton.Select();}
            if(result == GameResult.Failed) {_restartButton.Select();}
            if(result == GameResult.Won) {_endButton.Select();}
        });
    }

    private void SetButtons()
    {
        var result = GameManager.Instance.Stats.GameResult;
        _continueButton.onClick.RemoveAllListeners();
        _restartButton.onClick.RemoveAllListeners();
        _endButton.onClick.RemoveAllListeners();
        _endButton.onClick.AddListener(ContinueClicked);
        _continueButton.onClick.AddListener(ContinueClicked);
        _restartButton.onClick.AddListener(ContinueClicked);
        
        _continueButton.gameObject.SetActive(result == GameResult.NotDone);
        _restartButton.gameObject.SetActive(result == GameResult.Failed);
        _endButton.gameObject.SetActive(result == GameResult.Won);
    }

    private void SetTitle()
    {
        var result = GameManager.Instance.Stats.GameResult;
        _winTitle.SetActive(result != GameResult.Failed);
        _loseTitle.SetActive(result == GameResult.Failed);
    }
    
    private void ContinueClicked()
    {
        GameManager.Instance.Continue();
    }
}
