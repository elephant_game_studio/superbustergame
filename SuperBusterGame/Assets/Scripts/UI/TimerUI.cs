﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TimerUI : MonoBehaviour
{
    [SerializeField] private Text _timerText;

    private void Update()
    {
        var time = TimeSpan.FromSeconds(GameManager.Instance.Stats.LevelTime);
        _timerText.text = string.Format("{1:D2}:{2:D2}", 
            time.Hours, 
            time.Minutes, 
            time.Seconds, 
            time.Milliseconds);
    }
}
