﻿using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class LifesUI : MonoBehaviour 
{  
    [SerializeField] private Text _text;
    
    private void Start()
    {
        RefreshLifes(GameManager.Instance.Lifes);
        GameManager.Instance.OnLifesUpdate += RefreshLifes;
    }

    private void RefreshLifes(int value)
    {
        _text.text = value.ToString();
        DoUpdate();
    }

    [ShowInInspector]
    private void DoUpdate()
    {
        Sequence s = DOTween.Sequence();
        s
            .Append(_text.DOColor(Color.red, .4f))
            .Append(_text.DOColor(Color.white, .5f))
        ;
        _text.transform.DOPunchScale(-Vector3.one * .1f, .7f, 4);
    }
}
