﻿using UnityEngine;

public class Timer : MonoBehaviour
{
    private float _playTime;
    private GameManager _gameManager;
    
    private void Start()
    {
        _gameManager = GameManager.Instance;
    }
    
    private void Update()
    {
        if(_gameManager == null || _gameManager.Stats == null) { return; }
        if(_gameManager.GameState != GameState.Playing) { return; }

        _gameManager.Stats.LevelTime += Time.deltaTime;
    }
}
