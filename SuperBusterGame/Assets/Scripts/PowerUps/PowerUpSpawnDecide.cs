﻿using UnityEngine;

public class PowerUpSpawnDecide : MonoBehaviour
{
    [SerializeField] private PowerUpFactory _factory;

    private void Start()
    {
        KillManager.Instance.OnNewMonsterKill += DesideToSpawn;
    }

    private void DesideToSpawn(Monster obj)
    {
        //_factory.SpawnRandomPowerUp(obj.transform.position);

        if (Random.Range(0, 8) == 0)
            _factory.SpawnRandomPowerUp(obj.transform.position);
    }
}
