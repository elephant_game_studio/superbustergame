﻿using System;
using UnityEngine;

public class PowerUpTrigger : MonoBehaviour
{
    public event Action<PowerUp> OnNewPowerUp;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var powerUp = collision.GetComponent<PowerUp>();
        if (powerUp != null)
        {
            OnNewPowerUp?.Invoke(powerUp);
        }
    }
}
