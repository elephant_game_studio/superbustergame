﻿using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;


public class PowerUpResolver : MonoBehaviour
{
    private PowerUpTrigger _trigger;

    public event Action<PowerUpType, float> OnNewPowerUp;

    [SerializeField] private CanvasGroup _element;

    private void Start()
    {
        GameManager.Instance.OnNewLevelLoaded += LevelLoaded;
        _element.alpha = 0f;
    }

    private void LevelLoaded(int obj)
    {
        DisposeCurrent();
        if (_trigger != null)
            _trigger.OnNewPowerUp -= NewPowerUp;

        _trigger = GameManager.Instance.Player.GetComponent<PowerUpTrigger>();
        Debug.Assert(_trigger != null);
        _trigger.OnNewPowerUp += NewPowerUp;
    }

    private PowerUpLogic _currentPowerUp;

    private void DisposeCurrent()
    {
        if (_disableCorutine != null) StopCoroutine(_disableCorutine);
        if (_currentPowerUp != null)
        {
            _currentPowerUp.Dispose();
            OnNewPowerUp?.Invoke(PowerUpType.None, 0f);
            _element.DOFade(0f, .7f);
        }
    }
    

    private void NewPowerUp(PowerUp powerUp)
    {
        DisposeCurrent();
        switch (powerUp.Type)
        {
            case PowerUpType.Freez:
                _currentPowerUp = new FreezPowerUpLogic();
                _element.DOFade(.6f, .7f);
                break;
            case PowerUpType.CrossShoot:
                _currentPowerUp = new CrossShootPowerUpLogic();
                break;
            default:
                throw new ArgumentException($"none handled powerup type ({powerUp.Type})");
        }
        OnNewPowerUp?.Invoke(powerUp.Type, powerUp.LastTime);
        powerUp.DestroyMe();
        _disableCorutine = StartCoroutine(StartAfterTime(powerUp.LastTime, DisposeCurrent));
    }

    private Coroutine _disableCorutine;

    IEnumerator StartAfterTime(float time, Action toInvoke)
    {
        yield return new WaitForSeconds(time);
        toInvoke();
    }
}
