﻿public class CrossShootPowerUpLogic : PowerUpLogic
{
    private PlayerShooting _shooting;
    
    public CrossShootPowerUpLogic()
    {
        _shooting = GameManager.Instance.Player.GetComponent<PlayerShooting>();
        _shooting.UseCross = true;
    }

    public override void Dispose()
    {
        _shooting.UseCross = false;
    }
}
