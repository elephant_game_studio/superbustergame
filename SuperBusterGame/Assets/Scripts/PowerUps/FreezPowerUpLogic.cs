﻿using UnityEngine;

public class FreezPowerUpLogic : PowerUpLogic
{
    public FreezPowerUpLogic()
    {
        ReflectMove.allReflects.ForEach(x => x.Freez = true);
        ReflectMove.OnNewReflectMove += NewReflectMove;
    }

    public override void Dispose()
    {
        ReflectMove.OnNewReflectMove -= NewReflectMove;
        ReflectMove.allReflects.ForEach(x => x.Freez = false);
    }

    private void NewReflectMove(ReflectMove obj)
    {
        obj.Freez = true;
    }
}
