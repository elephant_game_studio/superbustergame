﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpFactory : SerializedMonoBehaviour
{
    [OdinSerialize]
    private Dictionary<PowerUpType, PowerUp> _prefabs;

    public void SpawnPowerUp(PowerUpType type, Vector2 pos)
    {
        if (type == PowerUpType.None)
        {
            return;
        }
        var powerUp = Instantiate(_prefabs[type]);
        powerUp.transform.position = pos;
    }

    public void SpawnRandomPowerUp(Vector2 pos)
    {
        int a = UnityEngine.Random.Range(0, Enum.GetValues(typeof(PowerUpType)).Length);
        SpawnPowerUp((PowerUpType)a, pos);
    }
}