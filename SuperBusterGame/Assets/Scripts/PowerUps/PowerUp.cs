﻿using System;
using System.Collections;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;

public enum PowerUpType { None, Freez, CrossShoot }
public abstract class PowerUp : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _sprite;
    [SerializeField] private float _time;
    public float LastTime => _time;

    public abstract PowerUpType Type { get; }

    [SerializeField] private float _destroyAfter;


    private void OnEnable()
    {
        Animate();
        StartCoroutine(DestoyCorutine(_destroyAfter));
    }

    private void Animate()
    {
        _sprite.transform.DOKill();
        _sprite.transform.DOLocalMove(new Vector3(0f, 0.15f, 0f), 1.5f).SetLoops(-1, LoopType.Yoyo);
    }
    
    IEnumerator DestoyCorutine(float time)
    {
        yield return new WaitForSeconds(time * .85f);
        StartBlink();
        yield return new WaitForSeconds(time * .15f);
        DestroyMe();
    }

    [ShowInInspector]
    private void StartBlink()
    {
        Sequence s = DOTween.Sequence();
        s
            .Append(_sprite.DOFade(0f, .3f))
            .Append(_sprite.DOFade(1f, .3f))
            .SetLoops(-1);
    }

    public void DestroyMe()
    {
        Destroy(gameObject);
    }
}
