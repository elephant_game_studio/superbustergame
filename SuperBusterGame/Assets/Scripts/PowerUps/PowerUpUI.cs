﻿using DG.Tweening;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpUI : SerializedMonoBehaviour
{
    [SerializeField] private PowerUpResolver _resolver;

    [SerializeField] private Sprite _crossShootIcon;
    [SerializeField] private Sprite _iceCubeIcon;

    [SerializeField] private RectTransform _view;
    [SerializeField] private Image _topImage;
    [SerializeField] private Image _backgroundImage;

    private float _length;
    
    private bool _visible;
    private bool Visible
    {
        get { return _visible; }
        set
        {
            //if (_visible = value) return;
            _visible = value;
            if (_visible)
                Show();
            else
                Hide();
        }
    }

    private Sprite Icon
    {
        set
        {
            if(value == null) {return;}
            _topImage.sprite = value;
            _backgroundImage.sprite = value;
            if (_topImage.sprite != null)
                _topImage.transform.parent.DOShakePosition(.8f, .7f, 4, 10);
        }
          
    }

    private void Start()
    {
        _resolver.OnNewPowerUp += NewPowerUp;
        _view.localScale = Vector3.zero;
    }

    private Tweener _tween;

    private void Show()
    {
        if (_tween != null) _tween.Kill();
        _view.gameObject.SetActive(true);
        _tween = (_view as Transform).DOScale(1f, .5f);
    }

    private void Hide()
    {
        _tween = (_view as Transform).DOScale(0f, .5f).OnComplete(() => _view.gameObject.SetActive(false));
    }

    private void NewPowerUp(PowerUpType type, float length)
    {
        Visible = type != PowerUpType.None;
        switch (type)
        {
            case PowerUpType.None:
                Icon = null;
                return;
                break;
            case PowerUpType.Freez:
                Icon = _iceCubeIcon;
                break;
            case PowerUpType.CrossShoot:
                Icon = _crossShootIcon;
                break;
            default:
                break;
        }

        _length = length;
        _topImage.fillAmount = 1;

        if (_countDown != null) StopCoroutine(_countDown);
        _countDown = StartCoroutine(CountDown());
    }

    Coroutine _countDown;

    IEnumerator CountDown()
    {
        var startTime = Time.time;
        while (_topImage.fillAmount >= 0)
        {
            _topImage.fillAmount = (_length - (Time.time - startTime)) / _length;
            yield return null;
        }
    }
}