﻿using System;

public abstract class PowerUpLogic : IDisposable
{
    public abstract void Dispose();
}
