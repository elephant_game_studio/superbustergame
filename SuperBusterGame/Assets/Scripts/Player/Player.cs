﻿using System;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour 
{
    //[SerializeField] Sprite 
    private bool _immortal;
    [SerializeField] private GameObject _viewRoot;
    [SerializeField] private GameObject _hitEffet;


    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (_immortal || !collision.gameObject.CompareTag("monster")) { return; }
        Die();
    }

    private void Die()
    {
        GameManager.Instance.ShakeCamera(5f);
        if (GameManager.Instance.PlayerHit())
        {
            BeImmortal(3f);
            var effect = Instantiate(_hitEffet, transform);
            effect.transform.localPosition = new Vector3(0, 1, 0);
            Destroy(effect.gameObject, 1);
        }
            
    }

    private void BeImmortal(float v)
    {
        _immortal = true;
        StartCoroutine(Blink(v));
    }

    IEnumerator Blink(float time)
    {
        float startTime = Time.time;
        while (time > Time.time - startTime)
        {
            yield return new WaitForSeconds(.3f);
            _viewRoot.SetActive(false);
            yield return new WaitForSeconds(.3f);
            _viewRoot.SetActive(true);
        }
        _immortal = false;
    }
}
