﻿using Sirenix.OdinInspector;
using System;
using UnityEngine;

[Serializable]
public class PlayerAnimationStateHolder
{
    private const string toWalk = "toWalk";
    private const string toIdle = "toIdle";

    public Transform holder;
    public Animator animator;

    public float idleSpeed;
    public float walkSpeed;

    private bool? _walk;
    public bool? Walk
    {
        get { return _walk; }
        set
        {
            if (_walk == value) return;
            Reset();
            _walk = value;
            if (_walk.Value) ToWalk();
            else ToIde();
        }
    }

    public void UpdateBlend(float value)
    {
        animator.SetFloat("blend", value);
        animator.speed = value > 0.1f ? walkSpeed * 3 : idleSpeed * 3;
    }
    
    
    public void Reset()
    {
        animator.ResetTrigger(toIdle);
        animator.ResetTrigger(toWalk);
    }

    [ShowInInspector]
    public void ToIde()
    {
        //Debug.Log($"toIdle in {animator.name}");
        animator.ResetTrigger(toWalk);
        animator.SetTrigger(toIdle);
        animator.speed = idleSpeed;
    }

    [ShowInInspector]
    public void ToWalk()
    {
        //Debug.Log($"toWalk in {animator.name}");
        animator.ResetTrigger(toIdle);
        animator.SetTrigger(toWalk);
        animator.speed = walkSpeed;
    }
}
