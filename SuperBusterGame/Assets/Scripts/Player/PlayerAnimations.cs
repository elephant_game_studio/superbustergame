﻿using System;
using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
    [SerializeField] private PlayerMovement _playerMovement;
    //[SerializeField] private SpriteRenderer _playerRenderer;
    //[SerializeField] private PlayerImages _playerImages;

    [SerializeField] private PlayerAnimationsHolder _animations;

    private PlayerAnimationStateHolder _current;

    private PlayerAnimationStateHolder Current
    {
        get { return _current; }
        set
        {
            if (_current == value) return;
            if (_current != null)
            {
                _current.Reset();
                _current.holder.gameObject.SetActive(false);
            }
            _current = value;
            _current.holder.gameObject.SetActive(true);
            //UpdateWalkIdle();
            //_current.ToIde();
        }
    }

    private Vector2 _lastDirection;

    private void OnEnable()
    {
        _animations.down.Reset();
        _animations.up.Reset();
        _animations.left.Reset();
        _animations.right.Reset();
        
        _animations.down.holder.gameObject.SetActive(true);
        _animations.up.holder.gameObject.SetActive(false);
        _animations.left.holder.gameObject.SetActive(false);
        _animations.right.holder.gameObject.SetActive(false);

        Current = _animations.down;
    }

    private void UpdateWalkIdle()
    {
        if (Current != null)
        {
            Current.UpdateBlend(Mathf.Abs(_playerMovement.Velocity)); //= Mathf.Abs(_playerMovement.Velocity) > 0f;
        }
    }

    private void Update()
    {
        if(GameManager.Instance.GameState != GameState.Playing) { return; }
        if (_playerMovement.Direction != _lastDirection)
        {

            _lastDirection = _playerMovement.Direction;
            if (_lastDirection == Vector2.down)
            {
                Current = _animations.down;
            }
            else if (_lastDirection == Vector2.up)
            {
                Current = _animations.up;
            }
            else if (_lastDirection == Vector2.left)
            {
                Current = _animations.left;
            }
            else if (_lastDirection == Vector2.right)
            {
                Current = _animations.right;
            }
        }
        UpdateWalkIdle();

    }
}

[Serializable]
public class PlayerAnimationsHolder
{
    public PlayerAnimationStateHolder up;
    public PlayerAnimationStateHolder down;
    public PlayerAnimationStateHolder left;
    public PlayerAnimationStateHolder right;
}