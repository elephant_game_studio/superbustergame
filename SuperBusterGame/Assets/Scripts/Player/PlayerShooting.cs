﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    [SerializeField] private PlayerMovement _playerMovement;
    [SerializeField] private Transform _bulletParent;
    [SerializeField] private Bullet _bulletPrefab; //TODO: change to factory with pool

    public bool UseCross { get; set; }

    private int BulletsToSpawn => UseCross ? 4 : 1;
    private List<Bullet> _bullets = new List<Bullet>();

    public event Action<Bullet> OnNewBullet;

    private void Awake()
    {
        if (_bulletParent == null) _bulletParent = transform.parent;
    }

    private void OnEnable()
    {
        _bullets = new List<Bullet>();
    }

    private void Update()
    {
        if(GameManager.Instance.GameState != GameState.Playing) { return; }
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        while (_bullets.Count > 0)
            _bullets[0].Destroy(gameObject);

        Vector2 dir = _playerMovement.Direction;
        for (int i = 0; i < BulletsToSpawn; i++)
        {
            var newBullet = Instantiate(_bulletPrefab, _bulletParent);
            newBullet.transform.position = transform.position;
            newBullet.Init(dir);
            newBullet.OnDestroyed += BulletDesttoyed;
            OnNewBullet?.Invoke(newBullet);
            _bullets.Add(newBullet);
            dir = dir.Rotate(90f);
        }
    }

    private void BulletDesttoyed(Bullet bullet, GameObject hit)
    {
        _bullets.Remove(bullet);
    }
}


