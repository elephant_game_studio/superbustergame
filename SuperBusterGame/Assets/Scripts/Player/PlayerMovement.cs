﻿using UnityEngine;
    
public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private float _moveSpeed;

    private float _horizontalInput;
    private float _verticalInput;
    
    public Vector2 Direction { get; private set; }
    public float Velocity { get; private set; }

    private void OnEnable()
    {
        Direction = Vector2.down;
        _rigidbody.velocity = Vector2.zero;
    }

    private void Start()
    {
        Direction = Vector2.down;
    }
    
    private void Update()
    {
        if(GameManager.Instance.GameState != GameState.Playing) { return; }
        ReadInputs();
        DefineDirection();
        MovePlayer();
    }

    private void ReadInputs()
    {
        _horizontalInput = Input.GetAxis("Horizontal");
        _verticalInput = Input.GetAxis("Vertical");
    }

    private void MovePlayer()
    {
        var currentPosition = transform.position;
        var newX = currentPosition.x + _horizontalInput * Time.deltaTime * _moveSpeed;
        var newY = currentPosition.y + _verticalInput * Time.deltaTime * _moveSpeed;
        var newPosition = new Vector2(newX, newY);
        
        _rigidbody.MovePosition(newPosition);
    }

    private void DefineDirection()
    {
        Vector2 input = new Vector2(_horizontalInput, _verticalInput);
        Velocity = _horizontalInput + _verticalInput;
        // ReSharper disable once CompareOfFloatsByEqualityOperator
        if(_horizontalInput == _verticalInput) { return; }

        if (Mathf.Abs(_horizontalInput) > Mathf.Abs(_verticalInput))
        {
            Direction = _horizontalInput > 0 ? Vector2.right  : Vector2.left;
        }
        else
        {
            Direction = _verticalInput > 0 ? Vector2.up : Vector2.down;
        }
    }
}


